<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api', 'namespace' => '\App\Http\Controllers\Api\V1'], function () use ($router) {
    $router->group(['prefix' => 'user'], function () use ($router) {
        $router->post('register', 'AuthController@registerUser');
        $router->post('sign-in', 'AuthController@signInUser');
        $router->post('recover-password', 'AuthController@recoverUserPassword');
        $router->patch('recover-password', 'AuthController@recoverUserPassword');

        $router->group(['middleware' => 'auth'], function () use ($router) {
            $router->post('companies', 'CompanyController@addUserCompany');
            $router->get('companies', 'CompanyController@getUserCompanies');
        });
    });
});
