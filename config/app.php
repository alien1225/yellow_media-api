<?php

return [
    'locale' => 'en',
    'jwt' => [
        'jwtKey' => env("JWT_KEY"),
        'jwtAlgo' => env('JWT_ALGO'),
        'jwtLifetime' => env('JWT_EXP')
    ]
];
