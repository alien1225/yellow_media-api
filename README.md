## Task

#### Clone rep

```bash
git clone https://gitlab.com/alien1225/yellow_media-api.git
```

#### Configure db, etc

```bash
cp .env.example .env
```

#### Migrate migrations

```bash
php artisan migrate
```

#### Run seeders

```bash
php artisan db:seed
```

#### Run local server

```bash
php -S localhost:8000 -t public
```
