<?php

namespace App\Repositories;

use App\Models\User;

interface UserRepositoryInterface
{
    public function getUserById(mixed $id): User;

    public function getUserByEmail(string $email): User;

    public function userExists(string $email): bool;
}
