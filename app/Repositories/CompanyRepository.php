<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

class CompanyRepository implements CompanyRepositoryInterface
{
    public function getUserCompanies(User $user): Collection
    {
        return $user->companies()->get();
    }
}
