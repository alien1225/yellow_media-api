<?php

namespace App\Exceptions\Model;

use RuntimeException;

class AlreadyExistsException extends RuntimeException
{
}
