<?php

namespace App\Exceptions;

use RuntimeException;

class AuthenticationMessageException extends RuntimeException
{
}
