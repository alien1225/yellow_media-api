<?php

namespace App\Authenticators;

use App\Exceptions\AuthenticationMessageException;
use App\Repositories\UserRepositoryInterface;
use App\Services\JwtService;
use Exception;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;

class JwtAuthenticator implements AuthenticatorInterface
{
    public function __construct(
        private JwtService $jwtService,
        private UserRepositoryInterface $userRepository
    ) {
    }

    public function authenticate(Request $request): Authenticatable
    {
        $jwtToken = $request->headers->get('Authorization');

        if (null === $jwtToken) {
            throw new AuthenticationMessageException('Authentication token is empty');
        }

        $jwtToken = str_replace('Bearer ', '', $jwtToken);
        try {
            $tokenData = $this->jwtService->decode($jwtToken);
        } catch (Exception $exception) {
            throw new AuthenticationMessageException('Token invalid');
        }

        try {
            $user = $this->userRepository->getUserById($tokenData->getUid());
        } catch (Exception $exception) {
            throw new AuthenticationMessageException('Token invalid');
        }

        return $user;
    }

    public function support(Request $request): bool
    {
        return $request->headers->has('Authorization');
    }
}
