<?php

namespace App\Authenticators;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;

interface AuthenticatorInterface
{
    public function authenticate(Request $request): Authenticatable;

    public function support(Request $request): bool;
}
