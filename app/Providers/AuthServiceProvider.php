<?php

namespace App\Providers;

use App\Authenticators\AuthenticatorInterface;
use App\Authenticators\JwtAuthenticator;
use App\Exceptions\AuthenticationMessageException;
use App\Models\User;
use App\Services\JwtService;
use App\Services\UserAuthService;
use Exception;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(JwtService::class, function ($app) {
            $jwtKey = config('app.jwt.jwtKey');
            $jwtAlgo = config('app.jwt.jwtAlgo');
            $jwtExp = config('app.jwt.jwtLifetime');

            return new JwtService($jwtKey, $jwtAlgo, $jwtExp);
        });

        $this->app->bind(AuthenticatorInterface::class, JwtAuthenticator::class);
    }

    public function boot(AuthenticatorInterface $authenticator): void
    {
        $this->app['auth']->viaRequest('api', function ($request) use ($authenticator) {
            $authEntity = null;

            if ($authenticator->support($request)) {
                try {
                    $authEntity = $authenticator->authenticate($request);
                } catch (AuthenticationMessageException $exception) {
                    return $authEntity;
                }
            }

            return $authEntity;
        });
    }
}
