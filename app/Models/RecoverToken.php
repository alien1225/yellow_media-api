<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class RecoverToken extends Model
{
    use HasFactory;

    protected $table = 'users_recover_token';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'token', 'expired_at',
        'user_id', 'created_at'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
