<?php

namespace App\Services;

use App\Models\Company;
use App\Models\User;

class CompanyService
{
    public function addUserCompany(User $user, array $companyData): Company
    {
        $company = Company::create($companyData);

        $user->companies()->save($company);

        return $company;
    }
}
