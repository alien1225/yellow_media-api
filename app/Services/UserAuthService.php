<?php

namespace App\Services;

use App\Exceptions\AuthenticationMessageException;
use App\Exceptions\Model\AlreadyExistsException;
use App\Models\RecoverToken;
use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use DateInterval;
use DateTimeImmutable;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserAuthService
{
    public function __construct(
        private UserRepositoryInterface $userRepository,
        private JwtService $jwtService
    ) {
    }

    public function signIn(string $email, string $password): string
    {
        try {
            $user = $this->userRepository->getUserByEmail($email);
        } catch (Exception $exception) {
            throw new AuthenticationMessageException('Access is prohibited');
        }

        if (!Hash::check($password, $user->getAuthPassword())) {
            throw new AuthenticationMessageException('Access is prohibited');
        }

        return $this->jwtService->encode($user);
    }

    public function register(array $userData): User
    {
        if ($this->userRepository->userExists($userData['email'])) {
            throw new AlreadyExistsException('User already exists');
        }

        $userData['password'] = Hash::make($userData['password']);

        return User::create($userData);
    }

    public function recoverPassword(string $email): void
    {
        if (!$this->userRepository->userExists($email)) {
            throw new ModelNotFoundException('No user was found with this email');
        }

        $user = $this->userRepository->getUserByEmail($email);
        $recoverToken = new RecoverToken();
        $recoverToken->token = Hash::make(Str::random());
        $createdAt = new DateTimeImmutable();
        $expiredAt = $createdAt->add(new DateInterval('P1D'));

        $recoverToken->created_at = $createdAt;
        $recoverToken->expired_at = $expiredAt;

        $user->recoverToken()->save($recoverToken);

        // Send an email with a token through the queues
    }
}
