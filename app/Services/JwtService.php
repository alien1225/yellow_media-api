<?php

namespace App\Services;

use App\DTO\JWT as JwtObject;
use App\Models\User;
use DateTimeImmutable;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class JwtService
{
    public function __construct(
        private string $jwtKey,
        private string $jwtAlgo,
        private string $jwtLifetime
    ) {
    }

    public function decode(string $token): JwtObject
    {
        $tokenData = (array) JWT::decode($token, new Key($this->jwtKey, $this->jwtAlgo));

        return (new JwtObject())
            ->setUid($tokenData['uid'])
            ->setIssuedAt($tokenData['iat'])
            ->setExpiredAt($tokenData['exp']);
    }

    public function encode(User $user): string
    {
        $issuedAt = new DateTimeImmutable();
        $expireAt = date_interval_create_from_date_string($this->jwtLifetime);
        if (false === $expireAt) {
            throw new Exception('Invalid date interval format in JWT.');
        }

        $expireAt = $issuedAt->add($expireAt);

        $tokenData = [
            'iat' => $issuedAt->getTimestamp(),
            'exp' => $expireAt->getTimestamp(),
            'uid' => $user->getAuthIdentifier(),
        ];

        return JWT::encode($tokenData, $this->jwtKey, $this->jwtAlgo);
    }
}
