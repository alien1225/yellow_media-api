<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rules\Password;
use Pearl\RequestValidate\RequestAbstract;

class UserRegisterRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'first_name' => ['required', 'max:255'],
            'last_name' => ['required', 'max:255'],
            'email' => ['required', 'email'],
            'password' => ['required', Password::min(6)],
            'phone' => ['required', 'max:255']
        ];
    }
}
