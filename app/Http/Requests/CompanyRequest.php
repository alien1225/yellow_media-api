<?php

namespace App\Http\Requests;

use Pearl\RequestValidate\RequestAbstract;

class CompanyRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'title' => ['required', 'max:255'],
            'phone' => ['required', 'max:255'],
            'description' => ['required', 'max:1024']
        ];
    }
}
