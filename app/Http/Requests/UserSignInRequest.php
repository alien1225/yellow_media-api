<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rules\Password;
use Pearl\RequestValidate\RequestAbstract;

class UserSignInRequest extends RequestAbstract
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => ['required', 'email'],
            'password' => ['required'],
        ];
    }
}
