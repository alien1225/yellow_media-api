<?php

namespace App\Http\Controllers\Api\V1;

use App\Exceptions\Model\AlreadyExistsException;
use App\Http\Requests\UserRecoverPasswordRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\UserSignInRequest;
use App\Services\UserAuthService;
use Exception;
use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends BaseController
{
    public function registerUser(UserRegisterRequest $request, UserAuthService $authService): JsonResponse
    {
        $reqData = $request->validated();

        try {
            $newUser = $authService->register($reqData);
        } catch (AlreadyExistsException $exception) {
            return response()->json(['message' => $exception->getMessage()], 422);
        }

        return response()->json(['data' => $newUser->toArray()]);
    }

    public function signInUser(UserSignInRequest $request, UserAuthService $authService): JsonResponse
    {
        $reqData = $request->validated();

        try {
            $jwt = $authService->signIn($reqData['email'], $reqData['password']);
        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 422);
        }

        return response()->json(['data' => ['access_token' => $jwt]]);
    }

    public function recoverUserPassword(UserRecoverPasswordRequest $request, UserAuthService $authService): JsonResponse
    {
        $reqData = $request->validated();

        try {
            $authService->recoverPassword($reqData['email']);
        } catch (Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 400);
        }

        return response()->json(['message' => 'OK'], 201);
    }
}
