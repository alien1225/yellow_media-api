<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Requests\CompanyRequest;
use App\Models\User;
use App\Repositories\CompanyRepositoryInterface;
use App\Services\CompanyService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyController
{
    public function getUserCompanies(Request $request, CompanyRepositoryInterface $companyRepository): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();
        $companies = $companyRepository->getUserCompanies($user)->toArray();

        return response()->json(['data' => $companies]);
    }

    public function addUserCompany(CompanyRequest $request, CompanyService $companyService): JsonResponse
    {
        /** @var User $user */
        $user = Auth::user();
        $reqData = $request->validated();
        $company = $companyService->addUserCompany($user, $reqData);

        return response()->json(['data' => $company->toArray()]);
    }
}
