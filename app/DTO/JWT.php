<?php

namespace App\DTO;

class JWT
{
    private int $uid;
    private int $expiredAt;
    private int $issuedAt;

    public function getUid(): int
    {
        return $this->uid;
    }

    public function getExpiredAt(): int
    {
        return $this->expiredAt;
    }

    public function getIssuedAt(): int
    {
        return $this->issuedAt;
    }

    public function setUid(int $uid): self
    {
        $this->uid = $uid;

        return $this;
    }

    public function setExpiredAt(int $expiredAt): self
    {
        $this->expiredAt = $expiredAt;

        return $this;
    }

    public function setIssuedAt(int $issuedAt): self
    {
        $this->issuedAt = $issuedAt;

        return $this;
    }
}
