<?php

namespace Database\Factories;

use App\Models\RecoverToken;
use DateInterval;
use DateTimeImmutable;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RecoverTokenFactory extends Factory
{
    protected $model = RecoverToken::class;

    public function definition(): array
    {
        $createdAt = new DateTimeImmutable();
        $expiredAt = $createdAt->add(new DateInterval('P1D'));

        return [
            'token' => Hash::make(Str::random()),
            'expired_at' => $expiredAt,
            'created_at' => $createdAt
        ];
    }
}
