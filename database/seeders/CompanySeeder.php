<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    public function run()
    {
        Company::factory()
            ->count(10)
            ->hasAttached(
                User::factory()->count(10),
            )
            ->create()
        ;
    }
}
