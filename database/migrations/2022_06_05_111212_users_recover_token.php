<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up()
    {
        Schema::create('users_recover_token', function (Blueprint $table) {
            $table->id('id');
            $table->string('token');
            $table->timestampTz('expired_at');
            $table->timestampTz('created_at');
            $table->foreignIdFor(User::class, 'user_id')->constrained();
        });
    }

    public function down()
    {
        Schema::drop('users_recover_token');
    }
};
