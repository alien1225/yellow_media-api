<?php

use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    protected $connection = 'pgsql';

    public function up(): void
    {
        Schema::create('users_companies', function (Blueprint $table) {
            $table->id('id');
            $table->foreignIdFor(User::class, 'user_id')->constrained();
            $table->foreignIdFor(Company::class, 'company_id')->constrained();
            $table->unique(['user_id', 'company_id']);
        });
    }

    public function down(): void
    {
        Schema::drop('users_companies');
    }
};
