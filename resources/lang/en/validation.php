<?php

return [
    'custom' => [
        'email' => [
            'required' => 'Email is required',
            'email' => 'Email is incorrect'
        ],
        'password' => [
            'required' => 'Password is required',
            'min' => [
                'string' => 'Password is incorrect'
            ]
        ],
        'first_name' => [
            'required' => 'First name is required',
            'max' => [
                'string' => 'First name is incorrect'
            ]
        ],
        'last_name' => [
            'required' => 'Last name is required',
            'max' => [
                'string' => 'Last name is incorrect'
            ]
        ],
        'phone' => [
            'required' => 'Phone is required',
            'max' => [
                'string' => 'Phone is incorrect'
            ]
        ],
        'title' => [
            'required' => 'Title is required',
            'max' => [
                'string' => 'Title is incorrect'
            ]
        ],
        'description' => [
            'required' => 'Description is required',
            'max' => [
                'string' => 'Description is incorrect'
            ]
        ]
    ]
];
